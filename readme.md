Guia Inbrands de Front-End
==========================

Esse documento traz as convenções que regem a escrita de código HTML e CSS no setor de Front-End da Inbrands, ilustradas com exemplos e imagens. O guia também descreve alguns princípios da [metodologia BEM](https://en.bem.info/methodology/key-concepts/) e como ela é aplicada em nosso dia-a-dia.

Ferramentas
-----------

Antes de começar a escrever código, sugerimos o uso de ferramentas que agilizam a automatizam parte do trabalho.

### SASS

O [SASS](http://sass-lang.com/) é um pre-processador que estende as capacidades do CSS com funcionalidades de linguagem de programação, como métodos e variáveis, e permite o aninhamento e a construção dinâmica de seletores, dentre outras funcionalidades.

```scss
/* Arquivo .scss original */

$docWidth: 95%;

.slideshow {
	width: $docWidth;

	&__media {
		display: block;
	}
	&__thumb {
		display: inline;
	}
}

/* Arquivo .css gerado */

.slideshow { width: 95%; }
.slideshow__media { display: block; }
.slideshow__thumb { display: inline; }
```

O SASS possui uma ferramenta online de processamento, o [SASS Meister](http://www.sassmeister.com/).

### PostCSS

O [PostCSS](https://github.com/postcss/postcss) é um pós-processador de CSS, ou seja, ele á aplicado depois que o CSS é gerado pelo SASS. Há vários plug-ins para PostCSS, dos quais destacamos o Autoprefixer e o Pixrem.

#### Autoprefixer

O [Autoprefixer](https://github.com/postcss/autoprefixer) aplica todos os prefixos necessários no CSS, tirando essa responsabilidade dos mixins do SASS, o que deixa o código-fonte mais legível e próximo da sintaxe do CSS. Configurável, o Autoprefixer conecta-se à base de dados do [Can I Use](http://caniuse.com/) para identificar quais prefixos são realmente necessários. Para usar Flexbox, é uma mão na roda! 

```scss
/* Arquivo .css gerado */

.parent {
    display: flex;
    flex-flow: row nowrap;
    justify-content: flex-start;
}

/* Arquivo .css pós-processado */

.parent {
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-flex-flow: row nowrap;
    -ms-flex-flow: row nowrap;
    flex-flow: row nowrap;
    -webkit-justify-content: flex-start;
    -ms-flex-pack: start;
    justify-content: flex-start;
}
```

O Autoprefixer possui uma [ferramenta online](https://autoprefixer.github.io/) de processamento.

#### Pixrem

O [Pixrem](https://github.com/robwierzbowski/node-pixrem) calcula e imprime no CSS o equivalente em pixels dos valores atribuídos na unidade de medida rem, mantendo, assim, a compatibilidade com navegadores mais antigos. Utilizamos em todos os nossos projetos responsivos.

```scss
/* Arquivo .css original */

.title: {
    font-size: 2rem;
}

/* Arquivo .css pós-processado */

.title: {
    font-size: 32px;
    font-size: 2rem;
}
```

#### Grunt

O [Grunt](http://gruntjs.com/) é o automatizador de tarefas que escolhemos para rodar os processos que transformam o código-fonte no código de produção. Com uma linha de comando no terminal, ele engatilha o SASS, o PostCSS e outras tarefas, tais como: incluir comentários com dados dinâmicos (como data e versão), combinar e "minificar" arquivos, processar imagens, enviar arquivos por FTP ou SFTP e mais.

##### Exemplos de processos comuns

* [Load Grunt Tasks](https://github.com/sindresorhus/load-grunt-tasks);
* [SASS](https://github.com/gruntjs/grunt-contrib-sass);
* [PostCSS](https://github.com/nDmitry/grunt-postcss);
* [Concat](https://github.com/gruntjs/grunt-contrib-concat);
* [Uglify](https://github.com/gruntjs/grunt-contrib-uglify);
* [Watch](https://github.com/gruntjs/grunt-contrib-watch).

#### Virtual Box e Modern IE

As lojas desenvolvidas na Inbrands têm suporte ao Internet Explorer a partir da versão 9. Para que seja possível realizar testes no navegador usando o Mac, utilizamos o [VirtualBox](https://www.virtualbox.org/), um software grátis de virtualização da Oracle.

O VirtualBox permite rodar máquinas virtuais do Windows com versões específicas do Internet Explorer instaladas, que são distribuidas pela própria Microsoft no site [Modern IE](https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/mac/).

Metodologia BEM
---------------

O [BEM](https://en.bem.info/methodology/) é uma metodologia de escrita de HTML, CSS e Javascript que preza pela facilidade de leitura, pela padronização e pelo reaproveitamento de código, desenvolvida pela empresa russa Yandex e adotada por outras grandes empresas como Shopify e Airbnb.

A palavra BEM é a sigla para Bloco, Elemento e Modificador, os três conceitos-chave que são usados para descrever todos os componentes de interface de um projeto.

O ato de **descrever** é, aliás, o primeiro passo ao iniciar qualquer projeto na metodologia BEM. Na Inbrands, antes de abrirmos o Sublime Text, costumamos imprimir e analisar os wireframes e lay-outs em busca de Blocos, Elementos e Modificadores.

### Bloco

O Bloco é o componente construtor do BEM, é a unidade que agrupa e orquestra Elementos, Modificadores e outros Blocos numa finalidade comum.

##### Um componente é um Bloco se...

* Representa uma funcionalidade completa;
* Pode ser reaproveitado em diferentes áreas do projeto;
* É composto por partes que interagem entre si.

![Bloco](http://www.richards.com.br/bem/bloco.jpg)

##### Exemplos

* Um menu;
* Um slideshow;
* Um conjunto de abas;
* A área que apresenta um produto — com foto, seletores e descrição;
* A área que descreve um preço — com valor cheio, valor promocional e opções de parcelamento.

### Elemento

O Elemento é o componente construtor dos Blocos, é a unidade que ganha significado apenas em conjunto e interagindo com outros componentes dentro do mesmo contexto.

##### Um componente é um Elemento se...

* Não representa, sozinho, uma funcionalidade completa;
* Perde a função caso seja removido do contexto em que está inserido.

![Elemento](http://www.richards.com.br/bem/elemento.jpg)

##### Exemplos

* Um botão de um menu;
* Uma imagem de um slideshow;
* Uma única aba de um conjunto de abas;
* Um único seletor de tamanho de uma área que apresenta um produto;
* O valor promocional dentro de uma área que apresenta as variações de um preço.

### Modificador

O Modificador representa uma variação de estado ou de aparência em Blocos ou Elementos.

##### Um componente possui um Modificador se...

* Apresenta características específicas como nome ou uma referência vinda do banco de dados;
* Apresenta variações de estado de acordo com a interação do usuário;
* Apresenta variações de aparência de acordo com a área em que está inserido.

![Elemento](http://www.richards.com.br/bem/modificador.jpg?neue)

##### Exemplos

* O estado selecionado de um botão;
* A apresentação de uma listagem de produtos em duas ou três colunas;
* A cor de fundo específica de uma página de conteúdo especial;
* A mudança de tamanho de um preço no somatório de uma tabela;
* As mudanças de formato de um menu quando aplicado no cabeçalho e no rodapé.

### Inception :-)

É importante ressaltar que Blocos podem ser incorporados dentro de outros Blocos. Por exemplo, um Bloco de navegação por thumbnails pode ser aplicado tanto num Bloco de slideshow quanto num Bloco de apresentacão de um produto.

![Blocos dentro de Blocos](http://www.richards.com.br/bem/inception.jpg)

A fase de identificação dos componentes é muito importante e deve ser feita com cuidado. Nem sempre é fácil identificar se um componente dentro de um Bloco é um Elemento ou um outro Bloco; ou se uma variação de um Bloco é tão diferente do original que configura a criação de um novo, ao invés da aplicação de um Modificador.


Tradução dos componentes em código
----------------------------------

Blocos, Elementos e Modificadores são traduzidos em código de maneiras distintas. Em comum, têm a característica de serem representados, no CSS, por classes e não ids. O BEM preza pela baixa especificidade dos seletores de CSS, que é a chave para a extensão dos componentes e o seu reaproveitamento modular. O uso de seletores de baixa especificidade praticamente elimina a necessidade do uso de `!important`, considerado uma má prática.

No HTML, as classes são sempre aplicadas, o que desassocia a estilização dos componentes de suas tags; assim, um mesmo componenete pode ser codificado utilizando tags diferentes de acordo com o contexto. O lado negativo dessa abordagem é que o HTML costuma ser maior do que o normal, mas é também descritivo e semântico.

A categorização dos componentes também influencia na forma como o projeto é divido em arquivos.

### Bloco

##### CSS

O Bloco é representado no CSS por uma classe formada por uma ou mais palavras em [camel case](https://pt.wikipedia.org/wiki/CamelCase). Por exemplo: `.menu` e `.miniCart`. O uso de sublinhados é reservado para o Elemento e o de hifens, para o Modificador.

```scss
/* Arquivo _menu.scss */

.menu {
	display: block;
	margin: 0;
	font-family: sans-serif;
}
```

##### HTML

O Bloco é representado no HTML por um tag que contém um grupo de Elementos, Modificadores e outros Blocos.

```html
<div class="menu">
	<!-- Outros componentes entram aqui -->
</div>
```

##### Arquivos

Em nossa implementação do BEM, cada Bloco corresponde a, pelo menos, um arquivo de SASS. Também é possível quebrar a definição de um Bloco em vários arquivos, como será visto a seguir.

### Elemento

##### CSS

O Elemento é representado no CSS por uma classe formada pelo nome do Bloco em que está inserido, seguido de dois sublinhados e uma ou mais palavras em camel case, representando seu nome. Por exemplo: `.menu__handle` e `.miniCart__area`.

Deve-se ter o cuidado de **nunca** representar toda a hierarquia de Elementos descrita no HTML. Por exemplo: a classe de um botão, dentro de um painel, dentro de um menu não é `.menu__panel__button`, e sim `.menu__button`.

No SASS, é permitido utilizar o conector `&` para concatenar e gerar os seletores dinamicamente.

```scss
/* Arquivo _menu.scss */

.menu {
	display: block;
	margin: 0;
	font-family: sans-serif;
	
	&__target {
		margin: 0;
		padding: 0;
		text-align: left;
	}
	&__item {
		list-style: none;
	}
	&__handle {
		display: block;
		padding: 0.5rem;
		color: #999;
		background-color: #e3e3e3;
	}
	&__link {
		color: #f90;
		font-size: 0.6875rem;
		text-transform: uppercase;
	}
}

/* Arquivo gerado menu.css */

.menu { display: block; margin: 0; font-family: sans-serif; }
.menu__target { margin: 0; padding: 0; text-align: left; }
.menu__item { list-style: none; }
.menu__handle { display: block; padding: 0.5rem; color: #999; background-color: #e3e3e3; }
.menu__link { color: #f90; font-size: 0.6875rem; text-transform: uppercase; }
```

##### HTML

O Elemento é representado no HTML necessariamente contido por um Bloco, **nenhum** Elemento pode existir solto no HTML, ou dentro de um Bloco ao qual não pertence. Sendo assim, a imagem `<img src="img.jpg" class="slideshow__media" alt=""/>` só poderia existir no Bloco `<div class="slideshow"></div>`.

É permitido aninhar Elementos em diferentes níveis, no entanto, e existência de muitos Elementos dentro de Elementos no HTML pode ser um indício que alguns deles são, na verdade, Blocos.

```html
<div class="menu">
	<ul class="menu__target">
		<li class="menu__item">
			<a class="menu__handle" href="/masculino">Masculino</a>
			<ul class="menu__target">
				<li class="menu__item">
					<a class="menu__link" href="/masculino/camisas">Camisas</a>
				</li>
				<li class="menu__item">
					<a class="menu__link" href="/masculino/calcas">Calças</a>
				</li>
			</ul>
		</li>
		<li class="menu__item">
			<a class="menu__handle" href="/feminino">Feminino</a>
			<ul class="menu__target">
				<li class="menu__item">
					<a class="menu__link" href="/feminino/camisas">Camisas</a>
				</li>
				<li class="menu__item">
					<a class="menu__link" href="/feminino/calcas">Calças</a>
				</li>
			</ul>
		</li>
	</ul>
</div>
```

##### Arquivos

Em nossa implementação do BEM, os Elementos não correspondem a arquivos separados, ainda que na metodologia original essa granularidade seja estimulada.

### Modificador

##### CSS

Há dois tipos de Modificadores no BEM, o que representa uma propriedade boleana, ou seja, que pode ser verdadeira ou falsa, e o que representa uma propriedade e um valor. Ambos são representados no CSS por uma classe que começa com o nome do Bloco ou do Elemento modificado, seguido de dois hifens e uma ou mais palavras em camel case. Apenas no Modificador de propriedade, há ainda um outro hífen seguido do valor da propriedade modificada.

Por exemplo, digamos que os estados possíveis de um botão sejam ativo e inativo. Nesse caso, a representação do botão ativo no CSS poderia ser a classe `.menu__button--active`, e a do botão normal, apenas `.menu__button`, sem o Modificador.

Em outro exemplo, digamos que uma listagem de produtos seja apresentada ao usuário em duas, três ou quatro colunas. Nesse caso, as classes que representam o Bloco modificado poderiam ser `.productList--numCols-2`, `.productList--numCols-3` e `.productList--numCols-4`.

Utiliza-se os Modificadores também para dar nomes e descrever características. Pode-se identificar cada produto de uma listagem com um Modificador que representa seu id no banco de dados, por exemplo, `.productList__item--id-8945`.

Os Modificadores também são usados para diferenciar as variações de um mesmo componente dependendo do contexto em que está inserido. Por exemplo, um menu, quando aplicado no cabeçalho da página, pode ser modificado pela classe `.menu--header` e, no footer, pela classe `.menu--footer`.

É comum que Modificadores sejam adicionados ao código via Javascript. Caso a manutenção do padrão `.componente--modificador` gere um grande esforço de programação, é possível utilizar Modificadores "soltos" que comecem com a palavra "is" como, por exemplo, `.is--active`. No entanto, esse artifício foge da metodologia BEM e **deve ser evitado**.

**Não é permitido** adicionar mais de um Modificador na mesma classe. Por exemplo, um botão que está ativo e que leva para uma promoção, não deve ter a classe `.banner__button--active--promo`, mas a combinação de duas classes `.banner__button--active` e `.banner__button--promo`.

A ordem dos componentes nas classes deve ser `.bloco__elemento--modificador`. A combinação `.bloco--modificador__elemento` não existe no BEM.

```scss
/* Arquivo _menu.scss */

.menu {
	display: block;
	margin: 0;
	font-family: sans-serif;
	
	&__target {
		margin: 0;
		padding: 0;
		text-align: left;

		&--main {
			display: flex;
			flex-flow: row nowrap;
			justify-content: flex-start;
		}
		&--sub {
			position: absolute;
			display: block;
			top: auto;
			left: -19999px;
		}
		@at-root {
			#{&}--sub#{&}--active {
				left: 0;
			}
		}
	}
	&__item {
		list-style: none;
		
		&--main {
			position: relative;
			flex: 0 1 8rem;
			text-align: center;
			margin: 0 0.5rem;
		}
	}
	&__handle {
		display: block;
		padding: 0.5rem;
		color: #999;
		background-color: #e3e3e3;
		
		&--active {
			color: #fff;
			background-color: #f90;
		}
	}
	&__link {
		color: #f90;
		font-size: 0.6875rem;
		text-transform: uppercase;
	}
}


/* Arquivo gerado menu.css */

.menu { display: block; margin: 0; font-family: sans-serif; }
.menu__target { margin: 0; padding: 0; text-align: left; }
.menu__target--main { display: flex; flex-flow: row nowrap; justify-content: flex-start; }
.menu__target--sub { position: absolute; display: block; top: auto; left: -19999px; }
.menu__target--sub.menu__target--active { left: 0; }
.menu__item { list-style: none; }
.menu__item--main { position: relative; flex: 0 1 8rem; text-align: center; margin: 0 0.5rem; }
.menu__handle { display: block; padding: 0.5rem; color: #999; background-color: #e3e3e3; }
.menu__handle--active { color: #fff; background-color: #f90; }
.menu__link { color: #f90; font-size: 0.6875rem; text-transform: uppercase; }
```

##### HTML

O Modificador é representado no HTML como uma classe combinada à classe original do Bloco ou do Elemento que modifica, não devendo substituí-la. Por exemplo, no caso de um botão ativo, teremos o atributo `class="nav__button nav__button--active"` e não apenas `class="nav__button--active"`.

```html
<div class="menu menu--header">
	<ul class="menu__target menu__target--main">
		<li class="menu__item menu__item--main">
			<a class="menu__handle" href="/masculino">Masculino</a>
			<ul class="menu__target menu__target--sub">
				<li class="menu__item menu__item--sub">
					<a class="menu__link" href="/masculino/camisas">Camisas</a>
				</li>
				<li class="menu__item menu__item--sub">
					<a class="menu__link" href="/masculino/calcas">Calças</a>
				</li>
			</ul>
		</li>
		<li class="menu__item menu__item--main">
			<a class="menu__handle" href="/feminino">Feminino</a>
			<ul class="menu__target menu__target--sub">
				<li class="menu__item menu__item--sub">
					<a class="menu__link" href="/feminino/camisas">Camisas</a>
				</li>
				<li class="menu__item menu__item--sub">
					<a class="menu__link" href="/feminino/calcas">Calças</a>
				</li>
			</ul>
		</li>
	</ul>
</div>
```

##### Arquivos

Em nossa implementação do BEM, os Modificadores não correspondem a arquivos separados, ainda que na metodologia original essa granularidade seja estimulada.

### Inception :-)

##### HTML

No BEM, é possível incluir Blocos dentro de Blocos. Por exemplo, um menu que inclui um slideshow pode ser representado no HTML conforme o exemplo a seguir.

```html
<div class="menu">
    <ul class="menu__target">
        <li class="menu__item">
            <a class="menu__handle" href="/masculino">Masculino</a>
            <ul class="menu__list"></ul>
            <div class="menu__area">
                <!-- bloco filho começa aqui -->
                <div class="slideshow">
                    <div class="slideshow__list">
                        <div class="slideshow__item slideshow__item--1"></div>
                        <div class="slideshow__item slideshow__item--2"></div>
                        <div class="slideshow__item slideshow__item--3"></div>
                    </div>
                    <ul class="slideshow__nav"></ul>
                </div>
                <!-- bloco filho termina aqui -->
            </div>
        </li>
    </ul>
</div>
```

É muito importante notar que, dentro da área do Bloco filho, cessam as classes que pertencem ao Bloco pai, ou seja, no contexto de `<div class="slideshow">` não há nenhuma classe que começe com `.menu`.

Só há um caso em que é permitido que as classes de dois Blocos se misturem, uma prática chamada BEM Mix, que consiste em nomear um Elemento do Bloco pai na mesma tag em que começa um Bloco filho. O mesmo exemplo anterior, escrito com um BEM Mix, teria um `<div>` a menos.

```html
<div class="menu">
    <ul class="menu__target">
        <li class="menu__item">
            <a class="menu__handle" href="/masculino">Masculino</a>
            <ul class="menu__list"></ul>
            <!-- BEM Mix / bloco filho começa aqui -->
            <div class="menu__area slideshow">
                <div class="slideshow__list">
                    <div class="slideshow__item slideshow__item--1"></div>
                    <div class="slideshow__item slideshow__item--2"></div>
                    <div class="slideshow__item slideshow__item--3"></div>
                </div>
                <ul class="slideshow__nav"></ul>
            </div>
            <!-- bloco filho termina aqui -->
        </li>
    </ul>
</div>
```

### Nomenclatura de classes

A proibição em misturar as classes dos Blocos (salvo no BEM Mix) levanta uma questão importante: como utilizar sistemas de grid, que são tão comuns em frameworks de CSS?

O exemplo a seguir é **incorreto**:

```scss
/* Proibido */

.row { display: flex; flex-flow: row wrap; }
.col-md-1 { width: 12.5%; }
.col-md-2 { width: 25%; }
.col-md-4 { width: 50%; }
.col-md-8 { width: 100%; }
```
```html
<!-- Proibido -->

<div class="banner row">
    <div class="banner__area col-md-4"></div>
    <div class="banner__area col-md-2"></div>
    <div class="banner__area col-md-2"></div>
</div>
```

Primeiramente, deve-se ter em mente que o BEM preza pelo uso de classes semânticas, que descrevem componentes e estados, mas não sua aparência. Classes como `.button--blue`, `.area--left` e `.col-md-8` carregam em seus nomes propriedades visuais que podem mudar no decorrer do projeto e, por isso, não podem ser utilizadas.

Por exemplo, se o botão `.button--blue` deixar de ser azul, a mudança terá que ser feita tanto no HTML quanto no CSS, e não apenas no CSS. O mesmo problema ocorre se a área `.area--left` for transferida para o lado direito ou se a região `.col-md-8` passar a ocupar quatro colunas do grid.

Para resolver os problemas anteriores, bastaria substituir a classe `.button--blue` por `.button--secondary` e a classe `.area--left` por `.area--aside`, mas ainda teríamos o problema das classes `.col-md-x`.

A solução que utilizamos em nossa implementação do BEM depende dos recursos do SASS [seletor placeholder](http://thesassway.com/intermediate/understanding-placeholder-selectors) e [importacão de partials](http://sass-lang.com/guide#topic-5). Os seletores placeholder, cuja sintaxe começa com o caracter `%`, são definidos e processados no SASS, mas não são impressos no código gerado, a menos que sejam aplicados em outras classes pela diretiva [`@extend`](http://sass-lang.com/documentation/file.SASS_REFERENCE.html#extend).

O exemplo a seguir é **correto**:

```scss
/* Arquivo _col.scss */

%col { display: flex; flex-flow: row wrap; }
%col__area--1 { width: 12.5%; }
%col__area--2 { width: 25%; }
%col__area--4 { width: 50%; }
%col__area--8 { width: 100%; }

/* Arquivo _banner.scss */

.banner {
    @extend %col;
    
    &__area {
        @extend %col__area--2;
        
        &--highlight {
            @extend %col__area--4;
        }
    }
}

/* Arquivo build.scss */

@import 'col';
@import 'banner';

/* Arquivo build.css gerado */

.banner { display: flex; flex-flow: row wrap; }
.banner__area { width: 25%; }
.banner__area--highlight { width: 50%; }
```
```html
<div class="banner">
    <div class="banner__area banner__area--highlight"></div>
    <div class="banner__area"></div>
    <div class="banner__area"></div>
</div>
```

É importante notar que, no exemplo, o seletor placeholder `%col` define um Bloco, e os demais seletores `%col__area--x` são Modificadores do Elemento `%col__area`.

### Hierarquia e estrutura de arquivos

A metodologia BEM sugere uma estrutura de arquivos que permite estender os Blocos (numa analogia à programação orientada ao objeto) ao dividí-los.

No exemplo a seguir, a definição do Header é feita em quatro arquivos diferentes: o primeiro, no nível mais alto, contém regras de CSS que são comuns a todos os projetos, o segundo, contém regras espefícifas para o Header do projeto RCH e o terceiro e o quarto contêm regras específicas de sua aplicação nas páginas de checkout e de produto.

```
/* Estrutura de pastas */

├─ common
│	└─ _header.scss
│	└─ _normalize.scss
└─ projects
	├─ bob
	├─ ell
	└─ rch
		├─ common
		│	└─ _header.scss
		├─ pages
		│	├─ collection
		│	├─ checkout
		│	│	└─ _header.scss
		│	└─ product
		│		└─ _header.scss
		└─ build.scss
```
```scss
/* Arquivo common/_header.scss */

.header {
    display: flex;
    flex-flow: row nowrap;
    
    &__area {
        flex: 1 1 20rem;
    }
}

/* Arquivo projects/rch/common/_header.scss */

.header {
    background-color: #e6e8eb;
}

/* Arquivo projects/rch/pages/checkout/_header.scss */

.header {
	.page--checkout & {
		&__area--main {
			display: none;
		}
	}
}

/* Arquivo projects/rch/pages/product/_header.scss */

.header {
	.page--product & {
		&__area--main {
			display: block;
		}
	}
}

/* Arquivo build.scss */

@import '../../common/header';
@import 'common/header';
@import 'pages/checkout/header';
@import 'pages/product/header';

/* Arquivo build.css gerado */

.header { display: flex; flex-flow: row nowrap; }
.header__area { flex: 1 1 20rem; }
.header { background-color: #e6e8eb; }
.page--checkout .header__area--main { display: none; }
.page--product .header__area--main { display: block; }
```

É muito importante ter cuidado ao escrever regras que serão aplicadas a vários projetos de uma vez. Em geral, o CSS escrito no nível mais alto da hierarquia contém apenas o mínimo essencial de cada Bloco.

Recomendações
-------------

### Bloco .page e Modificadores herdados do Bloco pai

No exemplo anterior, note o uso das classes `.page--checkout` e `.page--product`. Trata-se do Bloco `.page`, que é utilizado para descrever atributos das páginas que podem ser utilizadas para a estilização dos Blocos filhos e cujas classes devem ser aplicadas diretamente no `<body>`. Por exemplo: uma página de produto poderia ter as seguintes classes: `.page`, `.page--product`, `.page--product-3785`, `.page--category-masc` e por aí vai.

O Bloco `.page` é um exemplo de uso de Modificadores herdados do Bloco pai. A principal vantagem dessa prática é evitar que o mesmo Bloco tenha que ser codificado com um HTML específico de acordo com o contexto em que é aplicado, o que pode ser um problema em sistemas de gerenciamento de conteúdo.

O exemplo a seguir é **correto, mas não é preferido**:

```html
<body class="page page--checkout">
    <div class="menu"></div>
</body>
```
```scss
.page--checkout .menu { background-color: #e6e8eb; }
```

Quando for possível, prefira o uso de Modificadores específicos, que geram classes de CSS de menor especificidade, um dos princípios da metodologia BEM.

O exemplo a seguir é **correto e preferido**:

```html
<body class="page page--checkout">
    <div class="menu menu--checkout"></div>
</body>
```
```scss
.menu--checkout { background-color: #e6e8eb; }
```

No SASS, os Modificadores herdados do Bloco pai devem ser definidos no arquivo do Bloco filho.

O exemplo a seguir é **incorreto**:

```scss
/* Proibido */

/* Arquivo _page.scss */

.page--product {
    .menu {
        background-color: #e6e8eb;
    }
}

/* Arquivo .css gerado */

.page--product .menu { background-color: #e6e8eb; }
```

O exemplo a seguir é **correto**:

```scss
/* Arquivo _menu.scss */

.menu {
    .page--product & {
        background-color: #e6e8eb;
    }
}

/* Arquivo .css gerado */

.page--product .menu { background-color: #e6e8eb; }
```

Apesar de o primeiro e o segundo exemplos gerarem o mesmo CSS, apenas o segundo exemplo é correto porque é o Bloco filho que tem a responsabilidade de saber como se comporta dependendo do contexto em que é inserido.

### Regras de CSS para tags HTML

A metodologia BEM se opõe ao uso de tags HTML sendo utilizadas como seletores de CSS e, na Inbrands, estamos de acordo com esse limitação. Em nossos sites **não utilizamos** regras genéricas como:

```scss
/* Proibido */

img {
    width: 100%;
}

/* Proibido, sorry Paul Irish */

html {
    box-sizing: border-box;
}

*, *:before, *:after {
    box-sizing: inherit;
}
```

O reset de box-sizing descrito no exemplo anterior é uma prática bastante difundida e adotada, mas deve ser substituída por uma regra local aplicada ao Bloco, e não ao documento como um todo.

A única excessão à regra fica por conta do [Normalize.css](https://necolas.github.io/normalize.css/), um pequeno arquivo de CSS que serve para padronizar comportamentos inconsistentes entre os navegadores. O Normalize.css deve ser utilizado como se fosse um Bloco, no início CSS gerado.

### Bibliotecas de Javascript com seletores CSS fora do padrão BEM

Algumas bibliotecas de Javascript geram ou dependem de CSS próprio. O [Modernizr](https://modernizr.com/), por exemplo, gera uma série de classes no `<html>` de acordo com as capacidades do navegador. Outras bibliotecas que costumamos utilizar, como o [Swiper](http://idangero.us/swiper) e o [Magnific Popup](http://dimsemenov.com/plugins/magnific-popup/), geram e dependem de CSS próprio.

Em casos como esse, é permitido utilizar classes fora do padrão BEM no CSS; não existe a necessidade de adaptá-las, mesmo que exista essa opção.

### Flexbox

O Flexbox é um módulo de CSS **pensado para a diagramação de lay-outs responsivos**. Antes de sua criação, questões de lay-out que deveriam ser simples de resolver, como centralizar um elemento verticalmente em relação a uma área ou distribuir elementos em colunas, exigiam do desenvolvedor um arsenal de técnicas tão criativas quanto improvisadas.

Adotados o chamado *Flexbox First*, em que os lay-outs são primeiro codificados com Flexbox e depois é feita a inclusão de código CSS extra para manter a compatibilidade com outros navegadores. Nos links a seguir há dois materias excelentes sobre esse princípio:

* [Putting Flexbox Into Practice](http://zomigi.com/downloads/Putting-Flexbox-Into-Practice_Blend-Conf_130907.pdf) de Zoe Mickley Gillenwater.
* [A Complete Guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) do CSS-Tricks;

Tomemos, por exemplo, a diagramação de produtos em colunas, após muitas tentativas, chegamos a um padrão que adotamos em nossos projetos. No exemplo a seguir, [disponível também no Codepen](http://codepen.io/leofavre/pen/rebzqX), experimente mudar as variáveis para alterar as carecterísticas do lay-out.

[![https://blog.codepen.io/wp-content/uploads/2012/06/EditOn-Codepen.png](https://blog.codepen.io/wp-content/uploads/2012/06/EditOn-Codepen.png)](http://codepen.io/leofavre/pen/rebzqX?editors=1100)

```scss
$pageMaxWidth: 1300px; // largura máxima da página
$productHorzGap: 16px; // espaço horiontal entre produtos
$productVertGap: 48px; // espaço vertical entre produtos

.landing {
	margin: 0 auto;
	max-width: $pageMaxWidth;
	overflow: hidden;
}

.collection {
	&__list {
		display: flex;
		flex-flow: row wrap;
		align-items: flex-start;
		margin: 0 #{-$productHorzGap / 2} #{-$productVertGap};
		padding: 0;
	}
	&__item {
		list-style: none;
		box-sizing: border-box;
		padding: 0 #{$productHorzGap / 2} #{$productVertGap};

		width: 25%; // 4 colunas
		@media (max-width: 1000px){ width: 33.333%; } // 3 colunas
		@media (max-width: 700px){ width: 50%; } // 2 colunas
		@media (max-width: 400px){ width: 100%; } // 1 coluna
	}
}

.product {
	&__media {
		display: block;
		width: 100%;
	}
	&__desc {
		margin: 0 auto;
		width: 80%;
		text-align: center;
	}
}
```
```html
<div class="landing">
	<div class="collection">
		<ul class="collection__list">
			<li class="collection__item product">
				<img class="product__media" src="http://placehold.it/400x500" alt=""/>
				<p class="product__desc">Calça Jeans Jogging Power Skinny.</p>
			</li>
			<li class="collection__item product">
				<img class="product__media" src="http://placehold.it/400x500" alt=""/>
				<p class="product__desc">Texto maior para mostrar que a altura da linha de produtos se adapta automaticamente.</p>
			</li>
			<li class="collection__item product">
				<img class="product__media" src="http://placehold.it/400x500" alt=""/>
				<p class="product__desc">Sem float ou clearfix.</p>
			</li>
		</ul>
	</div>
</div>
```

O código acima funciona bem nos navegadores mais novos, mas, para os mais antigos, é necessário fazer alguns ajustes. O Flexbox teve implementações diferentes desde que foi criado; notadamente, o Internet Explorer 10 perpetua uma sintaxe que já caiu em desuso.

Para que não seja necessário aprender e escrever sintaxes antigas, utilize o [Autoprefixer](https://autoprefixer.github.io/), que gera o CSS extra automaticamente. Como foi mencionado no início do documento, ele pode ser usado como um dos processos do [PostCSS](https://github.com/postcss/postcss) dentro do [Grunt](http://gruntjs.com/).

```scss
/* Antes do Autoprefixer */

.collection__list {
	display: flex;
	flex-flow: row wrap;
	align-items: flex-start;
}

/* Depois do Autoprefixer */

.collection__list {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-flow: row wrap;
    flex-flow: row wrap;
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: flex-start;
}
```

Finalmente, para garantir a compatibilidade com o Internet Explorer 9 (na verdade, a técnica funciona até no IE8), utilizamos `display: inline-block` no CSS **antes** das regras do Flexbox. Em navegadores novos, esse CSS extra será ignorado, portanto **não é necessário** detectar, com Modernizr, se o navegador suporta Flexbox ou não.

```scss
/* Arquivo _collection.scss */

$productHorzGap: 16px; // espaço horiontal entre produtos
$productVertGap: 48px; // espaço vertical entre produtos

.collection {
	&__list {
		/* compatibilidade com IE8+ */
		word-spacing: -1em;

        /* flexbox para navegadores novos */
		display: flex;
		flex-flow: row wrap;
		align-items: flex-start;
		
		/* para todos os navegadores */
		margin: 0 #{-$productHorzGap / 2} #{-$productVertGap};
		padding: 0;
	}
	&__item {
		/* compatibilidade com IE8+ */
		display: inline-block;
		vertical-align: top;
		word-spacing: normal;

        /* para todos os navegadores */
		list-style: none;
		box-sizing: border-box;
		padding: 0 #{$productHorzGap / 2} #{$productVertGap};

        /* responsivo */
		width: 25%; // 4 colunas
		@media (max-width: 1000px){ width: 33.333%; } // 3 colunas
		@media (max-width: 700px){ width: 50%; } // 2 colunas
		@media (max-width: 400px){ width: 100%; } // 1 coluna
	}
}
```
```scss
/* Arquivo .css compatível com IE8+, gerado pelo SASS e depois processado pelo Autoprefixer */

.collection__list {
	/* compatibilidade com IE8+ */
	word-spacing: -1em;
	
	/* flexbox para navegadores novos */
	display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-flex-flow: row wrap;
    -ms-flex-flow: row wrap;
    flex-flow: row wrap;
    -webkit-box-align: start;
    -webkit-align-items: flex-start;
    -ms-flex-align: start;
	align-items: flex-start;
	
	/* para todos os navegadores */
	margin: 0 -8px -48px;
	padding: 0;
}
.collection__item {
	/* compatibilidade com IE8+ */
	display: inline-block;
	vertical-align: top;
	word-spacing: normal;
	
	/* para todos os navegadores */
	list-style: none;
	box-sizing: border-box;
	padding: 0 8px 48px;
	
	/* responsivo */
	width: 25%;
}
@media (max-width: 1000px) {
	.collection__item {
		width: 33.333%;
	}
}
@media (max-width: 700px) {
	.collection__item {
		width: 50%;
	}
}
@media (max-width: 400px) {
	.collection__item {
		width: 100%;
	}
}
```

O resultado final do primeiro exemplo compatível com navegadores antigos pode ser [conferido no Codepen](http://codepen.io/leofavre/pen/oxRLwa?editors=1100).

[![https://blog.codepen.io/wp-content/uploads/2012/06/EditOn-Codepen.png](https://blog.codepen.io/wp-content/uploads/2012/06/EditOn-Codepen.png)](http://codepen.io/leofavre/pen/oxRLwa?editors=1100)

Resumo
------

```scss
/* Faça */

.nomeComposto {}

/* Não faça */

.nome-composto {}
.nome_composto {}


/* Faça */

.bloco {}
.bloco--modificador {}
.bloco--modificador-valor {}
.bloco__elemento {}
.bloco__elemento--modificador {}
.bloco__elemento--modificador-valor {}

/* Não faça */

.bloco--modificador__elemento {}
.bloco__elementoX__elementoY {}


/* Faça */

.bloco--modificadorA.bloco--modificadorB {}

/* Não faça */

.bloco--modificadorA--modificadorB {}
.bloco--modificadorB--modificadorA {}


/* Faça */

.bloco {
    &__elementoX {}
    &__elementoY {}
    &__elementoZ {}
}

.bloco {}
.bloco__elementoX {}
.bloco__elementoY {}
.bloco__elementoZ {}

/* Não faça */

.bloco {
    &__elementoX {
        &__elementoY {
            &__elementoZ {}
        }
    }
}

.bloco {}
.bloco__elementoX {}
.bloco__elementoX__elementoY {}
.bloco__elementoX__elementoY__elementoZ {}


/* Faça */

%grid__area--4 {}
%grid__area--8 {}

@import 'grid';
.banner__area { @extend %grid__area--4; }

/* Não faça */

.grid__area--4 {}
.grid__area--8 {}


/* Prefira */

.menu--checkout {}

/* Evite */

.page--checkout .menu {}
```
```html
<!-- Faça -->

<div class="slideshow">
    <img class="slideshow__media" alt="" />
</div>

<!-- Não faça -->

<div class="product">
    <img class="slideshow__media" alt="" />
</div>


<!-- Faça -->

<div class="menu menu--header"></div>

<!-- Não faça -->

<div class="menu--header"></div>


<!-- Prefira -->

<a class="menu__button menu__button--active" href="/produtos"></a>

<!-- Evite -->

<a class="menu__button is--active" href="/produtos"></a>
```

---

o .caminho__do--bem